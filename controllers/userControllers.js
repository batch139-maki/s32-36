const User = require("./../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("./../models/Courses")

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	
	return User.findOne({email: reqBody.email}).then( (result, error) => {
		if(result != null){
			return `User already exists`
		} else {
			return newUser.save().then( (result, error) => {
				if(result){
					return true
				} else {
					return error
				}
			})
		}
	})
}


module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

module.exports.login = (reqBody) => {
  const { email, password } = reqBody;
  return User.findOne({ email: email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(password, result.password);
      console.log(result)
      if (isPasswordCorrect == true) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  })
}


module.exports.getProfile = (user) => {
    
    return User.findById(user.id).then((result, err) => {
        if(result != null){
            result.password = "";
            return result
        } else {
            return false
        }
    })
}

module.exports.enroll = async (data) => {
	const {userId, courseId} = data

	 const userEnroll = await User.findById(userId).then( (result, err) => {
		if(err){
			return err
		} else {
			// add the courseId in the enrollments array
			result.enrollements.push({courseId: courseId})

			if(result.enrollements.courseId != courseId){
				return result.save().then( (result, err) => {
					if(err){
						return err
					} else {
						return true
					}
				})
			}
			// save the changes made to the document

		}
	})

	// look for matching document of a course
	const courseEnroll = await Course.findById(courseId).then( (result, err) => {
		if(err){
			return err
		} else {
			// add the userId in the enrollees array
			result.enrollees.push({userId: userId})
			if(result.enrollements.userId != userId){
				return result.save().then( (result,err) => {
					if(err){
						return err
					} else {
						return true
					}
				})
			}
		}
	})
}

