const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");

//register a user
// http://localhost:4000/api/users
router.post("/register", (req, res) => {

	userController.register(req.body).then( result => res.send(result))
})

router.get("/", (req, res) => {

	userController.getAllUsers().then( result => res.send(result))
})

router.post("/login", (req, res) => {
  userController.login(req.body).then( result => res.send(result))
})

router.get("/details", (req, res) => {
    console.log(req)
    let userData = auth.decode(req.headers.authorization);
    console.log(userData)
    userController.getProfile(userData).then((result) => res.send(result))
})

router.post("/enroll", auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    courseId: req.body.courseId
  }

  userController.enroll(data).then(result => res.send(result))
})


module.exports = router;
